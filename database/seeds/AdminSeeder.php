<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$user = User::where('name', 'root')->first();

    	if (!$user) {
	        User::create([
	        	'name' => 'root',
	        	'is_admin' => true,
	        	'email' => 'root@test.com',
	        	'password' => Hash::make('123456'),
	        ]);
    	}
    }
}
