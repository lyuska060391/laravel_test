<?php

use App\Order;
use App\Status;
use App\Product;
use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ["Борис", "Иванов", "465465465654"],
            ["Кирилл", "Щур", "465465465654"],
            ["Максим", "Пигалов", "465465465654"],
            ["Борис", "Иванов", "465465465654"],
            ["Кирилл", "Щур", "465465465654"],
            ["Максим", "Пигалов", "465465465654"],
            ["Борис", "Иванов", "465465465654"],
            ["Кирилл", "Щур", "465465465654"],
            ["Максим", "Пигалов", "465465465654"],
            ["Борис", "Иванов", "465465465654"],
            ["Кирилл", "Щур", "465465465654"],
            ["Максим", "Пигалов", "465465465654"],
        ];

        foreach ($data as $key => $person) {
            $status = Status::all()->random(); 
            $product = Product::all()->random();
            if ($status && $product) {
                $count = rand(1, 5);
                $price = $count * $product->price;
                $order = Order::create([
                    'first_name' => $person[0],
                    'last_name' => $person[1],
                    'phone' => $person[2],
                    'status_id' => $status->id,
                    'price' => $price,
                ]);
                if ($order) {
                    DB::table('order_product')->insert([
                        'order_id' => $order->id,
                        'product_id' => $product->id,
                        'count' => $count,
                    ]);
                }
            }
        }
    }
}
