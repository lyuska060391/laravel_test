<?php

use App\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['Джинсы', 4654 ],
            ['Кофта', 46546 ],
            ['Юбка', 9855 ],
            ['Рубашка', 63655 ],
            ['Куртка', 6555 ],
            ['Носки', 546 ],
            ['Брюки', 6876 ],
            ['Шорты', 798 ],
            ['Легинсы', 654 ],
            ['Платье', 7987 ],
            ['Майка', 879 ],
            ['Кепка', 6545 ],
        ];

        foreach ($data as $item) {
            Product::create([
            	'name' => $item[0],
            	'price' => $item[1],
            	'image' => 'images/no_image.png',
            ]);
        }
        
    }
}
