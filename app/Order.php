<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
	protected $fillable = [
		'first_name',
		'last_name',
		'phone',
        'price',
        'status_id',
	];

    public function status()
    {
    	return $this->belongsTo(Status::class);
    }

    public function products()
    {
    	return $this->belongsToMany(Product::class)->withPivot('count');
    }
}
