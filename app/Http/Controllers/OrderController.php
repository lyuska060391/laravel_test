<?php

namespace App\Http\Controllers;

use App\Order;
use App\Status;
use App\Product;
use App\Http\Requests\OrderRequest;
use DB;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Order::paginate(env('PAGE_SIZE', 10));
        return view('order.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $statuses = Status::all();
        $products = Product::all();
        return view('order.create', ['statuses' => $statuses, 'products' => $products]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrderRequest $request)
    {
        $data = $request->validated();
        $data['price'] = 0;
        $order = Order::create($data);
        return redirect()->route('order.edit', $order->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        $statuses = Status::all();
        $products = Product::all();
        return view('order.edit', ['order' => $order, 'statuses' => $statuses, 'products' => $products]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(OrderRequest $request, Order $order)
    {
        $data = $request->validated();

        $total = 0;
        foreach ($order->products as $product) {
            $total += $product->price * $product->pivot->count;
        }

        if ($data['is_add_product'] == 'true' && !is_null($data['count'])) {
            DB::table('order_product')->insert([
                'order_id' => $order->id,
                'product_id' => (int)($data['product']),
                'count' => $data['count'],
            ]);

            $product = Product::find((int)($data['product']));
            $total += $product->price * $data['count'];
        }

        $data['price'] = $total;
        $order->update($data);
        return redirect()->route('order.edit', $order->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }
}
