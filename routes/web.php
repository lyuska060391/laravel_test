<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/status/{id}/destroy', 'StatusController@destroy')->name('status_destroy');
Route::resource('/status', 'StatusController');

Route::get('/product/{id}/destroy', 'ProductController@destroy')->name('product_destroy');
Route::resource('/product', 'ProductController');

Route::resource('/order', 'OrderController');
