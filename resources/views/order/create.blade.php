@extends('layouts.app')

@section('content')
<div class="container">
	<h1>Create</h1>
	<form action="{{ route('order.store') }}" method="post" enctype="multipart/form-data">
		{{ csrf_field() }}
		<div class="form-group">
			<label for="first_name">First Name</label>
			<input class="form-control {{ $errors->first('first_name') == '' ? '' : 'is-invalid' }}" type="text" name="first_name" id="first_name">
			<div class="invalid-feedback">
          		{{ $errors->first('first_name') }}
        	</div>
		</div>
		<div class="form-group">
			<label for="last_name">Last Name</label>
			<input class="form-control {{ $errors->first('last_name') == '' ? '' : 'is-invalid' }}" type="text" name="last_name" id="last_name">
			<div class="invalid-feedback">
          		{{ $errors->first('last_name') }}
        	</div>
		</div>
		<div class="form-group">
			<label for="phone">Phone</label>
			<input class="form-control {{ $errors->first('phone') == '' ? '' : 'is-invalid' }}" type="number" name="phone" id="phone">
			<div class="invalid-feedback">
          		{{ $errors->first('phone') }}
        	</div>
		</div>
		<div class="form-group">
		    <label for="status_id">Status</label>
		    <select class="form-control {{ $errors->first('status_id') == '' ? '' : 'is-invalid' }}" id="status_id" name="status_id">
		    	@foreach ($statuses as $status)
		    		<option value="{{$status->id}}">{{$status->name}}</option>
		        @endforeach
		    </select>
		    <div class="invalid-feedback">
          		{{ $errors->first('status_id') }}
        	</div>
		</div>

		<button type="submit" class="btn btn-success">Save</button>
	</form>
</div>
@endsection