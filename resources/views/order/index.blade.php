@extends('layouts.app')

@section('content')
<div class="container">
	<a href="{{ route('order.create') }}">Создать</a>
	<h1>Заказы</h1>
	<table class="table">
  		<thead>
    		<tr>
      			<th scope="col">#</th>

      			<th scope="col">First Name</th>
            <th scope="col">Last Name</th>

            <th scope="col">Phone</th>

            <th scope="col">Price</th>
            <th scope="col">Status</th>
      			<th scope="col">Edit</th>
    		</tr>
  		</thead>
  		<tbody>
			@foreach ($data as $order)
    		<tr>
    			<td>{{ $order->id }}</td>
    			<td>{{ $order->first_name}}</td>
          <td>{{ $order->last_name}}</td>
          <td>{{ $order->phone }}</td>
          <td>{{ $order->price }}</td>
          <td>{{ $order->status->name }}</td>
    			<td>
    				<a href="{{ route('order.edit', $order->id) }}">Изменить</a>
    			</td>
    		</tr>
			@endforeach
		</tbody>
	</table>
  {{ $data->links() }}
</div>
@endsection