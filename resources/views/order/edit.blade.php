@extends('layouts.app')

@section('content')
<div class="container">
	<h1>Edit</h1>
	<form action="{{ route('order.update', $order->id) }}" method="post" enctype="multipart/form-data" id="my_form">
		{{ csrf_field() }}
		<input name="_method" type="hidden" value="PUT">
		<input name="is_add_product" id="is_add_product" type="hidden" value="false">

		<div class="form-group">
			<label for="first">First Name</label>
			<input class="form-control" type="text" name="first_name" id="first_name" value="{{ $order->first_name }}">
		</div>
		<div class="form-group">
			<label for="last">Last Name</label>
			<input class="form-control" type="text" name="last_name" id="last_name" value="{{ $order->last_name }}">
		</div>
		<div class="form-group">
			<label for="phone">Phone</label>
			<input class="form-control" type="number" name="phone" id="phone" value="{{ $order->phone }}">
		</div>
		<div class="form-group">
			<label for="price">Price</label>
			<input class="form-control" type="number" name="price" id="price" value="{{ $order->price }}" disabled>
		</div>
		<div class="form-group">
		    <label for="status_id">Status</label>
		    <select class="form-control" id="status_id" name="status_id">
		    	@foreach ($statuses as $status)
		    		@if ($order->status_id == $status->id)
		      			<option selected value="{{$status->id}}">{{$status->name}}</option>
		      		@else
		      			<option value="{{$status->id}}">{{$status->name}}</option>
		      		@endif
		        @endforeach
		    </select>
		</div>
		<h2>Products</h2>
		<table class="table table-sm">
		  	<thead>
			    <tr>
			      	<th scope="col">Name</th>
			      	<th scope="col">Count</th>
			      	<th scope="col">Price</th>
			      	<th scope="col">Cost</th>
			    </tr>
		  	</thead>
		  	<tbody>
		  		@foreach ($order->products as $product)
		    	<tr>
		      		<th scope="row">{{$product->name}}</th>
		      		<td>{{$product->pivot->count}}</td>
		      		<td>{{$product->price}}</td>
		      		<td>{{$product->pivot->count * $product->price}}</td>
		    	</tr>
		    	@endforeach
		  	</tbody>
		</table>
		<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add_product">
 			Add product
		</button>

		<div class="modal" id="add_product">
			<div class="modal-dialog">
			    <div class="modal-content">
				    <div class="modal-header">
				        <h5 class="modal-title">Add Product</h5>
				        <button type="button" class="close" data-dismiss="modal">
				          	<span aria-hidden="true">&times;</span>
				        </button>
				    </div>
			      	<div class="modal-body">
			        	<div class="form-group">
						    <label for="product">Select Product</label>
						    <select class="form-control" id="product" name="product">
						    	@foreach ($products as $product)
						      		<option value="{{$product->id}}">{{$product->name}}</option>
						        @endforeach
						    </select>
						</div>

						<div class="form-group">
							<label for="count">Count</label>
							<input class="form-control" type="number" name="count" id="count">
						</div>
			      	</div>
			      	<div class="modal-footer">
			        	<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			        	<button type="button" class="btn btn-primary" onclick="document.getElementById('is_add_product').value='true'; document.getElementById('my_form').submit(); document.getElementById('is_add_product').innerHtml='false';">Add</button>
			        </div>
			    </div>
		    </div>
		</div>

		<button type="submit" class="btn btn-success">Save</button>
	</form>

</div>
@endsection