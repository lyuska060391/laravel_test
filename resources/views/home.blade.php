@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="list-group">
                        <a href="{{ route('status.index') }}" class="list-group-item list-group-item-action">Статусы</a>
                        <a href="{{ route('product.index') }}" class="list-group-item list-group-item-action">Товары</a>
                        <a href="{{ route('order.index') }}" class="list-group-item list-group-item-action">Заказы</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
