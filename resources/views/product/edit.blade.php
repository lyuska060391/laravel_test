@extends('layouts.app')

@section('content')
<div class="container">
	<h1>Edit</h1>
	<form action="{{ route('product.update', $product->id) }}" method="post" enctype="multipart/form-data">
		{{ csrf_field() }}
		<input name="_method" type="hidden" value="PUT">
		<div class="form-group">
			<label for="name">Name</label>
			<input class="form-control {{ $errors->first('name') == '' ? '' : 'is-invalid' }}" type="text" name="name" id="name" value="{{ $product->name }}">
			<div class="invalid-feedback">
          		{{ $errors->first('name') }}
        	</div>
		</div>
		<div class="form-group">
			<label for="price">Price</label>
			<input class="form-control {{ $errors->first('price') == '' ? '' : 'is-invalid' }}" type="number" name="price" id="price" value="{{ $product->price }}">
			<div class="invalid-feedback">
          		{{ $errors->first('price') }}
        	</div>
		</div>
		<div class="form-group">
			<label for="image">Image</label>
			<input class="form-control" type="file" name="image" id="image">
			<img src="{{ url('/storage/' . $product->image) }}" class="img-fluid img-thumbnail mt-2" width="200">
		</div>
		<button type="submit" class="btn btn-success">Save</button>
	</form>
</div>
@endsection