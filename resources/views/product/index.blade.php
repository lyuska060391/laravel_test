@extends('layouts.app')

@section('content')
<div class="container">
	<a href="{{ route('product.create') }}">Создать</a>
	<h1>Товары</h1>
	<table class="table">
  		<thead>
    		<tr>
      			<th scope="col">#</th>

      			<th scope="col">Name</th>
            <th scope="col">Price</th>

      			<th scope="col">Edit</th>
      			<th scope="col">Delete</th>
    		</tr>
  		</thead>
  		<tbody>
			@foreach ($data as $product)
    		<tr>
    			<td>{{ $product->id }}</td>
    			<td>{{ $product->name }}</td>
          <td>{{ $product->price }}</td>
    			<td>
    				<a href="{{ route('product.edit', $product->id) }}">Изменить</a>
    			</td>
    			<td>
    				<a href="{{ route('product_destroy', $product->id) }}">Удалить</a>
    			</td>
    		</tr>
			@endforeach
		</tbody>
	</table>
  {{ $data->links() }}
</div>
@endsection