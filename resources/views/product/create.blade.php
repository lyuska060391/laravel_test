@extends('layouts.app')

@section('content')
<div class="container">
	<h1>Create</h1>
	<form action="{{ route('product.store') }}" method="post" enctype="multipart/form-data">
		{{ csrf_field() }}
		<div class="form-group">
			<label for="name">Name</label>
			<input class="form-control {{ $errors->first('name') == '' ? '' : 'is-invalid' }}" type="text" name="name" id="name">
			<div class="invalid-feedback">
          		{{ $errors->first('name') }}
        	</div>
		</div>
		<div class="form-group">
			<label for="price">Price</label>
			<input class="form-control {{ $errors->first('price') == '' ? '' : 'is-invalid' }}" type="number" name="price" id="price">
			<div class="invalid-feedback">
          		{{ $errors->first('price') }}
        	</div>
		</div>
		<div class="form-group">
			<label for="image">Image</label>
			<input class="form-control" type="file" name="image" id="image">
		</div>
		<button type="submit" class="btn btn-success">Save</button>
	</form>
</div>
@endsection