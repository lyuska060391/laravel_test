@extends('layouts.app')

@section('content')
<div class="container">
	<h1>Edit</h1>
	<form action="{{ route('status.update', $status->id) }}" method="post">
		{{ csrf_field() }}
		<input name="_method" type="hidden" value="PUT">
		<div class="form-group">
			<label for="name">Name</label>
			<input class="form-control {{ $errors->first('name') == '' ? '' : 'is-invalid' }}" type="text" name="name" id="name" value="{{$status->name}}">
			<div class="invalid-feedback">
          		{{ $errors->first('name') }}
        	</div>
		</div>
		<button class="btn btn-success">Save</button>
	</form>
</div>
@endsection