@extends('layouts.app')

@section('content')
<div class="container">
	<a href="{{ route('status.create') }}">Создать</a>
	<h1>Статусы</h1>
	<table class="table">
  		<thead>
    		<tr>
      			<th scope="col">#</th>
      			<th scope="col">Name</th>
      			<th scope="col">Edit</th>
      			<th scope="col">Delete</th>
    		</tr>
  		</thead>
  		<tbody>
			@foreach ($data as $status)
    		<tr>
    			<td>{{ $status->id }}</td>
    			<td>{{ $status->name }}</td>
    			<td>
    				<a href="{{ route('status.edit', $status->id) }}">Изменить</a>
    			</td>
    			<td>
    				<a href="{{ route('status_destroy', $status->id) }}">Удалить</a>
    			</td>
    		</tr>
			@endforeach
		</tbody>
	</table>
  {{ $data->links() }}
</div>
@endsection