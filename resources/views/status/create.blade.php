@extends('layouts.app')

@section('content')
<div class="container">
	<h1>Create</h1>
	<form action="{{ route('status.store') }}" method="post">
		{{ csrf_field() }}
		<div class="form-group">
			<label for="name">Name</label>
			<input class="form-control {{ $errors->first('name') == '' ? '' : 'is-invalid' }}" type="text" name="name" id="name">
			<div class="invalid-feedback">
          		{{ $errors->first('name') }}
        	</div>
		</div>
		<button class="btn btn-success">Save</button>
	</form>
</div>
@endsection