@extends('layouts.app')

@section('content')
<div class="container">
	<h1>{{ $status->name }}</h1>
    <p>{{ $status->name }} with ID {{ $status->id }}</p>
</div>
@endsection